import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';



Template.test1.onCreated(function(){

	// Setup Reactive Var here so I can change the input type
	this.inputRole = new ReactiveVar('task');

});

Template.test1.onRendered(function (){
	console.log("Reactive State is + " + Template.instance().inputRole.get());

});

Template.test1.helpers({

	inputRole(role) {
		return Template.instance().inputRole.get() === role;
	},

});

Template.test1.events({

	'change #ibox'(event, instance) {
		console.log("change on ibox");
		console.dir(event);
		let current = Template.instance().inputRole.get();

		if (current === 'task') {
			//unload selectize.
			console.log ("In Task Part, just accept input and console log it");
			console.dir("The following task text was entered:" + $(event.target).val());

let $st = $('#ibox').selectize({
		    plugins: ['remove_button'],
		    persist: false,
		    maxItems: 2,
		    highlight: true,
		    valueField: 'tag',
		    labelField: 'name',
		    searchField: ['name', 'tag'],
		    // options = collection.find().fetch() - For Each
		    options: [
		      {tag: 'Marketing', name: 'Marketing'},
		      {tag: 'Business Operations', name: 'Business Operations'},
		      {tag: 'Finance', name: 'Finance'}
		      // Is there a way to trigger a function here to launch a ticket request for a user who feels a new value 
		      // should be added? I don't want anyone to be able to add a tag that isn't vetted first 
		    ],
		  });	

		  $('#ibox').focus();		

		}

		else if (current === 'category') {
			console.log ("In Category Part, just accept input and console log it");
			console.dir("The following category text was entered:" + $(event.target).val());
			// this is where I want to load selectize here so I can accept tags as input

		  


		}


		let next = current === 'task' ? 'category' : 'task';
		$(event.target).val('');
		Template.instance().inputRole.set(next);
	}

});








Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
